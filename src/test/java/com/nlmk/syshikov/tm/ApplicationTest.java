package com.nlmk.syshikov.tm;

import static org.junit.Assert.assertTrue;

import com.nlmk.sychikov.tm.Application;
import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.entity.Task;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ApplicationTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        final Application application = new Application();
        final Task task = application.getTaskService().findByIndex(0);
        System.out.println(task);
        final Project project = application.getProjectService().findByIndex(0);
        System.out.println(project);
        System.out.println("Adding task...");
        application.getProjectTaskService().addTaskToProject(project.getId(),task.getId());
        application.getProjectTaskService().addTaskToProject(project.getId(),application.getTaskService().findByIndex(1).getId());
        application.getTaskController().renderTasks(application.getProjectTaskService().findAllByProjectId(project.getId()));
        System.out.println("Removing one task...");
        application.getProjectTaskService().removeTaskFromProject(project.getId(),task.getId());
        application.getTaskController().renderTasks(application.getProjectTaskService().findAllByProjectId(project.getId()));
        System.out.println("Removing project...");
        application.getProjectTaskService().removeProjectByIndex(0);
        System.out.println(task);

        assertTrue(true);
    }
}
