package com.nlmk.sychikov.tm.repository;

import com.nlmk.sychikov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Projects storage
 */
public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project(name, description);
        projects.add(project);
        assignToUser(project, userId);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        return project;
    }

    public int getRepositorySize() {
        return projects.size();
    }

    public Project assignToUser(final Project project, final Long userId) {
        project.setUserId(userId);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public void clear(final Long userId) {
        final List<Project> userProjects = findAllByUserId(userId);
        if (userProjects == null) return;
        for (final Project project : userProjects) {
            projects.remove(project);
        }
    }

    public Project findByIndex(final int index) {
        return projects.get(index);
    }

    public Project findById(final Long id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    public Project findById(final Long id, final Long userId) {
        for (final Project project : projects) {
            if (id.equals(project.getId()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    public Project findByName(final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    public Project findByName(final String name, final Long userId) {
        for (final Project project : projects) {
            if (name.equals(project.getName()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeById(final Long id, final Long userId) {
        final Project project = findById(id, userId);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name, final Long userId) {
        final Project project = findByName(name, userId);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAllByUserId(final Long userId) {
        List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(project);
        }
        return result;
    }

}
