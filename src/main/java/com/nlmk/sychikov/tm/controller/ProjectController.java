package com.nlmk.sychikov.tm.controller;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.service.ProjectService;
import com.nlmk.sychikov.tm.service.ProjectTaskService;

import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    private final UserController userController;

    public ProjectController(
            final ProjectService projectService, final ProjectTaskService projectTaskService,
            final UserController userController
    ) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.userController = userController;
    }

    /**
     * Create new project to user
     *
     * @param userId project will be created for user with userId
     * @return return value
     */
    public int createProject(Long userId) {
        System.out.println("[CREATE PROJECT]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        if (userId == null) return -1;
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        if (projectService.create(name, description, userId) == null) {
            System.out.println("[FAILED]");
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Assign project to user
     *
     * @return return value
     */
    public int assignProjectToUserByIds() {
        System.out.println("[ASSIGN PROJECT TO USER BY IDS]");
        final Long projectId = longInputProcessor("Enter project id: ");
        if (projectId == -1)
            return -1;
        final Long userId = longInputProcessor("Enter user id: ");
        if (userId == -1)
            return -1;
        if (!checkUserPermission(projectId)) return -1;
        final Project project = projectService.assignToUser(projectId, userId);
        if (project == null) {
            System.out.println("[FAILED]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Add new project to current user
     *
     * @return return value
     */
    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        if (projectService.create(name, description, userController.getCurrentUserId()) == null) {
            System.out.println("[FAILED]");
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update project by id
     *
     * @return return value
     */
    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Project id='" + id.toString() + "' is not found!]");
            return 0;
        }
        if (!checkUserPermission(project.getId())) return -1;
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectService.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update project by index
     *
     * @return return value
     */
    public int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        final int index = intInputProcessor("Enter project index: ");
        if (index == -1)
            return 0;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Project with index='" + (index + 1) + "' is not found!]");
            return 0;
        }
        if (!checkUserPermission(project.getId())) return -1;
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by name
     *
     * @return return value
     */
    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("[Project '" + name + "' is not found!]");
            return -1;
        }
        if (!checkUserPermission(project.getId())) return -1;
        projectTaskService.removeProjectByName(name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by id
     *
     * @return return value
     */
    public int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return -1;
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Project with id=" + id.toString() + " is not found!]");
            return -1;
        }
        if (!checkUserPermission(project.getId())) return -1;
        projectTaskService.removeProjectById(project.getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by index
     *
     * @return return value
     */
    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        final int index = intInputProcessor("Enter project index: ");
        if (index == -1)
            return -1;
        Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Project with id=" + (index + 1) + " is not found!]");
            return -1;
        }
        if (!checkUserPermission(project.getId())) return -1;
        projectTaskService.removeProjectByIndex(index);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all projects
     *
     * @return return value
     */
    public int clearProject() {
        System.out.println("[CLEAR PROJECTS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * clear all project by user id
     *
     * @return return value
     */
    public int clearCurrentUserProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectTaskService.removeProjectByUserId(userController.getCurrentUserId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all projects
     *
     * @return return value
     */
    public int listAllProject() {
        System.out.println("[LIST ALL PROJECTS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        int index = 1;
        for (Project project : projectService.findAll()) {
            System.out.println(index + ". UID=" + project.getUserId() + ": " + project.getId() + ": " + project.getName() + ": " + project.getDescription());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all projects by current user
     *
     * @return return value
     */
    public int listProject() {
        System.out.println("[LIST PROJECTS]");
        int index = 1;
        final List<Project> projects = projectService.findAllByUserId(userController.getCurrentUserId());
        if (projects == null || projects.isEmpty()) return 0;
        for (Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + ": " + project.getDescription());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all projects by user id
     *
     * @return return value
     */
    public int listUserProject() {
        System.out.println("[LIST PROJECTS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        final Long userId = longInputProcessor("Enter user id: ");
        if (userId == -1)
            return -1;
        int index = 1;
        final List<Project> projects = projectService.findAllByUserId(userId);
        if (projects == null || projects.isEmpty()) return 0;
        for (Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + ": " + project.getDescription());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Show the project
     *
     * @param project what project should be printed
     */
    public void viewProject(final Project project) {
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Shows project by index
     *
     * @return return value
     */
    public int viewProjectByIndex() {
        final int index = intInputProcessor("Enter project index: ");
        if (index == -1)
            return -1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Project index='" + index + "' is not found!]");
            return -1;
        }
        if (!checkUserPermission(project.getId())) return -1;
        viewProject(project);
        return 0;
    }

    /**
     * Shows project by id
     *
     * @return return value
     */
    public int viewProjectById() {
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Project id='" + id.toString() + "' is not found!]");
            return 0;
        }
        if (!checkUserPermission(project.getId())) return -1;
        viewProject(project);
        return 0;
    }

    /**
     * Check does the user have ADMIN role or user is project owner
     * and print alarm if hasn't.
     *
     * @return true if has ADMIN role or user is current user.
     */
    private boolean checkUserPermission(Long projectId) {
        if (projectId == null) return false;
        final Project project = projectService.findById(projectId);
        if(project==null) return false;
        if (userController.getCurrentUser().getId().equals(project.getUserId())) return true;
        return userController.checkCurrentUserAdminPermissionAlarm();
    }

}
